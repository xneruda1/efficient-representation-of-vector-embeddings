import os
import sys

from utils import files

# from ..utils import files

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import h5py
import numpy as np


def h5_to_npy(h5_file: str, output_file: str) -> None:
    h5_data = h5py.File(h5_file, "r")
    data = h5_data['emb'][:]
    h5_data.close()
    np.save(output_file, data)
    print(f"Created {output_file}")


def convert(key):
    dataset_name = files.datasets_dict()[key]
    new_name = get_path(key)
    h5_to_npy(dataset_name, new_name)


def load(path):
    return np.load(path)


def get_path(size, home_dir=files.home_data_dir()):
    return f"{home_dir}/npy/npy_{size}.npy"


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def load_all():
    for size in files.dataset_keys():
        path = get_path(size)
        data = load(path)


@files.timer_func
def get_elements_npy(path, first=True, count=100):
    data = load(path)
    if first:
        return data[:count]
    return data[-count:]


def scratch_path(size):
    scratch_dir = os.environ["SCRATCHDIR"]
    return f"{scratch_dir}/npy_{size}.npy"


@files.timer_func
def get_every_nth(size, n, count):
    data = load(scratch_path(size))
    for i in range(0, len(data) - count, n):
        elements = data[n:n + count]
    return


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else scratch_path(size)
    data = load(path)
    for _ in range(n_times):
        data = np.append(data, array_of_elements, axis=0)
    np.save(path, data)


if __name__ == "__main__":
    key = sys.argv[1]
