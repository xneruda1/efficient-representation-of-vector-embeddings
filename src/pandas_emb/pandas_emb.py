import sys
import pandas
import os
from utils import files

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import h5py


def numpy_to_pandas(h5_file: str) -> pandas.DataFrame:
    file = h5py.File(h5_file, 'r')
    data = file['emb'][:]
    file.close()
    return pandas.DataFrame(data)


def save_to_h5(data, filename: str) -> None:
    file = h5py.File(filename, 'w')
    file.create_dataset('emb', data=data)
    file.close()


def convert(key):
    dataset_name = files.datasets_dict()[key]
    data_frame = numpy_to_pandas(dataset_name)
    new_name = f"{files.home_data_dir()}pandas/pandas_{key}.h5"
    save_to_h5(data_frame, new_name)
    print(f"created {new_name}")


def load(path):
    file = h5py.File(path, 'r')
    return pandas.DataFrame(file['emb'][:])


def get_path(size, home_dir=files.home_data_dir()):
    return f"{home_dir}/pandas/pandas_{size}.h5"


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def load_all():
    for key in files.dataset_keys():
        path = get_path(key)
        data = load(path)


@files.timer_func
def get_elements(path, first=True, count=100):
    with h5py.File(path, 'r') as file:
        dataset = file['emb']

        if first:
            return pandas.DataFrame(dataset[:count])
        return pandas.DataFrame(dataset[-count:])


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


def get_path_scratch(size):
    return f"{os.environ['SCRATCHDIR']}/pandas_{size}.h5"


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else get_path_scratch(size)
    data = load(path)
    converted_array = pandas.DataFrame(array_of_elements)
    for i in range(n_times):
        data = pandas.concat([data, converted_array])
    save_to_h5(data, path)


if __name__ == "__main__":
    key = sys.argv[1]
    print(key)
    convert(key)
