import gzip
import os
import pickle
import sys

import numpy as np
from utils import files
from h5 import generator

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import h5py


def pickle_dataset(file_name: str = 'pickle_no_compr.pkl'):
    dataset = generator.random_dataset()
    with open('../data/' + file_name, 'wb') as file:
        pickle.dump(dataset, file)


def pickle_dataset_gzip(file_name: str = 'pickle_emb.pkl.gzip'):
    dataset = generator.random_dataset()
    with gzip.open('../data/' + file_name, 'wb') as file:
        pickle.dump(dataset, file)


def h5_to_pickle(h5_file: str, output_file: str) -> None:
    h5_data = h5py.File(h5_file, "r")
    data = h5_data['emb'][:]
    h5_data.close()
    with open(output_file, "wb") as f:
        pickle.dump(data, f)
        print(f"created {output_file}")


def convert(size):
    dataset_name = files.datasets_dict()[size]
    output_file = get_path(size)
    h5_to_pickle(dataset_name, output_file)


def load(path):
    with open(path, "rb") as file:
        data = pickle.load(file)
    return data


def get_path(size, home_dir=files.home_data_dir()):
    return f"{home_dir}/pickle/{size}.pickle_emb"


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def load_all():
    for size in files.dataset_keys():
        data = load_size(size)


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


def get_path_scratch(size):
    return f"{os.environ['SCRATCHDIR']}/{size}.pickle_emb"


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else get_path_scratch(size)
    data = load(path)
    for i in range(n_times):
        data = np.append(data, array_of_elements, axis=0)
    with open(path, "wb") as file:
        pickle.dump(data, file)


if __name__ == "__main__":
    key = sys.argv[1]
    print(key)
    convert(key)
