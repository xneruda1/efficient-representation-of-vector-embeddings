from time import time
import os
import shutil


def datasets_list():
    return ["/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=100K.h5",
            "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=300K.h5",
            "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=10M.h5",
            "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=30M.h5",
            "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=100M.h5"]


def datasets_dict():
    return {"100K": "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=100K.h5",
            "300K": "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=300K.h5",
            "10M": "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=10M.h5",
            "30M": "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=30M.h5",
            "100M": "/storage/brno12-cerit/home/prochazka/datasets/sisap23/laion2B-en-clip768v2-n=100M.h5"}


def dataset_names():
    return {"100K": "laion2B-en-clip768v2-n=100K.h5",
            "300K": "laion2B-en-clip768v2-n=300K.h5",
            "10M": "laion2B-en-clip768v2-n=10M.h5",
            "30M": "laion2B-en-clip768v2-n=30M.h5",
            "100M": "laion2B-en-clip768v2-n=100M.h5"}


def dataset_keys():
    return datasets_dict().keys()


def home_data_dir():
    return "/storage/brno12-cerit/home/jakubneruda/datasets/"


def copy_to_scratch(src_path, scratch_dir):
    filename = os.path.basename(src_path)
    dest_path = os.path.join(scratch_dir, filename)
    try:
        shutil.copy(src_path, dest_path)
        print(f"File '{filename}' copied successfully to '{dest_path}'.")
    except Exception as e:
        print(f"Error: Unable to copy file '{filename}' to '{dest_path}'.")
        print(f"Exception: {e}")


def timer_func(func):
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'{func.__name__!r}{args} executed in {(t2 - t1):.4f}s')
        return f'{(t2 - t1):.4f}'

    return wrap_func
