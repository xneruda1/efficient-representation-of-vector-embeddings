import os

import numpy as np

import zarr_main

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import sys

sys.path.append("/storage/brno12-cerit/home/jakubneruda/efficient-representation-of-vector-embeddings/src")
from subprocess import Popen
import h5.load
import pickle_emb.pickle_emb
from pandas_emb import pandas_emb
from pickle_emb import pickle_emb
from pyTorch_emb import torch_emb
from pyTables_emb import pyTables_emb
from npy_emb import npy_emb
from datetime import datetime
import gc
from utils import files


def convert_size(key):
    npy_emb.convert(key)
    pickle_emb.convert(key)
    pandas_emb.convert(key)
    torch_emb.convert(key)
    pyTables_emb.convert(key)
    zarr_main.convert(key)


def load_all():
    torch_emb.load_all()
    pandas_emb.load_all()
    npy_emb.load_all()
    pickle_emb.load_all()
    h5.load.load_all()


def load_datasets_by_format(format_name='h5'):
    if format_name == 'h5':
        h5.load.load_all()
    elif format_name == 'pickle':
        pickle_emb.load_all()
    elif format_name == 'npy':
        npy_emb.load_all()
    elif format_name == 'pandas':
        pandas_emb.load_all()
    elif format_name == 'pyTorch_emb':
        torch_emb.load_all()
    else:
        print('invalid format')


def load_datasets_by_size(size='100K', scratch_dir=os.environ["SCRATCHDIR"], from_scratch=False):
    if not from_scratch:
        scratch_dir = None
    h5.load.load_size(size, scratch_dir=scratch_dir)
    gc.collect()
    npy_emb.load_size(size, scratch_dir=scratch_dir)
    gc.collect()
    pandas_emb.load_size(size, scratch_dir=scratch_dir)
    gc.collect()
    pickle_emb.load_size(size, scratch_dir=scratch_dir)
    gc.collect()
    torch_emb.load_size(size, scratch_dir=scratch_dir)
    gc.collect()


def copy_datasets_to_scratch(size, scratch_dir=os.environ["SCRATCHDIR"]):
    torch_emb.copy_to_scratch(size, scratch_dir)
    pyTables_emb.copy_to_scratch(size, scratch_dir)
    pickle_emb.copy_to_scratch(size, scratch_dir)
    pandas_emb.copy_to_scratch(size, scratch_dir)
    npy_emb.copy_to_scratch(size, scratch_dir)
    h5.load.copy_to_scratch(size, scratch_dir)


def get_every_nth_element(size, n, count):
    pyTables_emb.get_every_nth(size, n, count)
    # npy_emb.get_every_nth(size, n, count)


def append_to_dataset(dataset_size, count, n_times=1):
    rng = np.random.default_rng(12345)
    arr = rng.random(dtype=np.float32, size=(count, 768))

    pyTables_emb.append(dataset_size, arr, n_times)
    zarr_main.append_zarr(dataset_size, arr, n_times)
    h5.load.append(dataset_size, arr, n_times)
    torch_emb.append(dataset_size, arr, n_times)
    npy_emb.append(dataset_size, arr, n_times)
    pandas_emb.append(dataset_size, arr, n_times)
    pickle_emb.append(dataset_size, arr, n_times)


def mprof_by_size(
        output_dir='/storage/brno12-cerit/home/jakubneruda/efficient-representation-of-vector-embeddings/graphs/data',
        src_dir='/storage/brno12-cerit/home/jakubneruda/efficient-representation-of-vector-embeddings/src',
        sizes=files.dataset_keys(),
        scratch_dir=""):
    for ds_size in sizes:
        date = datetime.now().strftime("%m-%d")
        t = datetime.now().strftime("%H-%M-%S")
        out_name = f"{output_dir}/{date}_{ds_size}_{t}.dat"
        print(f"mprof out: {out_name}")
        Popen(f"mprof run -o {out_name} python3 {src_dir}/main.py {ds_size} {scratch_dir}",
              shell=True).wait()
        gc.collect()


def get_first_last_elements(dataset_size='10K'):
    pass


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("missing argument (mprof, convert, {data_size})")
        exit()
    arg1 = sys.argv[1]
    if arg1 == 'mprof':
        if len(sys.argv) <= 2:
            mprof_by_size()
        elif len(sys.argv) > 3 and sys.argv[2] in files.dataset_keys():
            mprof_by_size(sizes={sys.argv[2]}, scratch_dir=sys.argv[3])
        elif sys.argv[2] in files.dataset_keys():
            mprof_by_size(sizes={sys.argv[2]})

    elif arg1 == 'convert':
        size = sys.argv[2]
        if size in files.dataset_keys():
            convert_size(size)
        else:
            print(f"invalid argument 2: {size}")
    elif arg1 == 'copy':
        if len(sys.argv) > 2:
            copy_datasets_to_scratch(sys.argv[2])
        else:
            copy_datasets_to_scratch('100K')
    elif arg1 == 'append':
        if len(sys.argv) > 2:
            append_to_dataset(sys.argv[2], 1000)
        else:
            append_to_dataset('100K', 1000)
    elif arg1 in files.dataset_keys():
        if len(sys.argv) > 2:
            load_datasets_by_size(arg1, sys.argv[2])
        else:
            load_datasets_by_size(arg1)
    else:
        print(f"invalid argument {arg1}")
