import shutil
import sys
import os

from memory_profiler import profile
import torch

from utils import files

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import h5py


def h5_to_torch(h5_file: str, output_file: str) -> None:
    h5_data = h5py.File(h5_file, "r")
    data = h5_data['emb'][:]
    h5_data.close()
    data = torch.from_numpy(data)
    torch.save(data, output_file)
    print(f"Created {output_file}")


def convert(dataset_key):
    dataset_name = files.datasets_dict()[dataset_key]
    output_file = get_path(dataset_key)
    h5_to_torch(dataset_name, output_file)


def load(path):
    return torch.load(path)


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def get_path(size='100K', home_dir=files.home_data_dir()):
    return f"{home_dir}/pytorch/{size}.pt"


def get_path_scratch(size='100K'):
    # return get_path(size, home_dir=os.environ["SCRATCHDIR"])
    scratch_dir = os.environ["SCRATCHDIR"]
    return f"{scratch_dir}/{size}.pt"


def load_all():
    for size in files.dataset_keys():
        path = get_path(size)
        data = load(path)


# @profile(precision=4)
@files.timer_func
def get_elements_torch(dataset_path, first=True, count=100):
    data = load(dataset_path)
    if first:
        return data[:count]
    return data[-count:]


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else get_path_scratch(size)
    data = load(path)
    converted_data = torch.tensor(array_of_elements)
    for i in range(n_times):
        data = torch.cat([data, converted_data])
    torch.save(data, path)
    print(f"Appended {len(array_of_elements) * n_times} elements to {path}")


if __name__ == "__main__":
    key = sys.argv[1]
    print(key)
    convert(key)
