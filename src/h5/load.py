import gzip
import os
import pickle
import h5py
import re

import numpy as np
import psutil
from datetime import datetime
from subprocess import Popen

from time import time

from utils import files


# from memory_profiler import profile
# https://pypi.org/project/memory-profiler/


def human_readable(num, suffix="B"):
    for unit in ("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"):
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


def timer_func(func):
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'{func.__name__!r}{args} executed in {(t2 - t1):.4f}s')
        return f'{(t2 - t1):.4f}'

    return wrap_func


@timer_func
def load_h5_dataset(path, n=1):
    for i in range(n):
        file = h5py.File(path, 'r')
        data = file['emb'][:]


def load(path):
    file = h5py.File(path, 'r')
    return file['emb'][:]


# @timer_func
# @profile(precision=4)
def load_actual_h5_dataset(path, n=1):
    print()
    print(f'loading h5 dataset')
    process = psutil.Process(os.getpid())
    for i in range(n):
        file = h5py.File(path, 'r')
        # print(file['emb'])
        data = file['emb'][:]
        print("Memory usage of the entire proccess:", human_readable(process.memory_info().rss))
        print("Memory size of numpy array in bytes:", human_readable(data.size * data.itemsize))


# @profile(precision=4)
@timer_func
def load_actual_h5_dataset_by_chunks(path, n=1, chunk=1000):
    print()
    print(f'loading h5 dataset by chunks (chunk={chunk})')
    process = psutil.Process(os.getpid())
    for i in range(n):
        file = h5py.File(path, 'r')
        # print(file['emb'])
        memory_usage_list = []
        j = 0
        data = []
        for k in range(0, file['emb'].len(), chunk):
            data = file['emb'][j:k]
            j = k
            memory_usage_list.append(process.memory_info().rss)

        print("Memory usage average of the entire proccess:", human_readable(np.average(memory_usage_list)))
        print("Memory size of numpy array in bytes:", human_readable(data.size * data.itemsize))


@timer_func
def load_pickle_dataset(path):
    for i in range(10):
        with open(path, 'rb') as file:
            data = pickle.load(file)


@timer_func
def load_gzip_pickle_dataset(path):
    for i in range(10):
        with gzip.open(path, 'rb') as file:
            data = pickle.load(file)


def load_all_data(path="../data"):
    with open('../../metadata/time_to_load.csv', 'w') as output:
        output.write('name,seconds\n')
        for filename in os.listdir(path):
            s = -1
            if re.search("\.h5$", filename):
                s = load_h5_dataset(path + '/' + filename)
            elif re.search("\.pkl$", filename):
                s = load_pickle_dataset(path + '/' + filename)
            elif re.search("\.pkl\.gzip$", filename):
                s = load_gzip_pickle_dataset(path + '/' + filename)

            output.write(f'{filename},{s}\n')
            print(f'{filename},{s}\n')


def load_all_remote_datasets():
    path = '/storage/brno12-cerit/home/prochazka/datasets/sisap23/'
    name_prefix = "laion2B-en-clip768v2"
    for file in os.listdir(path):
        if file.startswith(name_prefix):
            dt = datetime.now().strftime("%d/%m/%Y-%H:%M:%S")
            print(file, dt)
            print(f"mprof run python3 load.py {path}{file} -o ../graphs/{file}-{dt}.png")
            Popen(f"echo 'a' > /storage/brno12-cerit/home/jakubneruda/scripts/log.txt", shell=True).wait()
            Popen(f"mprof run python3 load.py {path}{file} -o ../graphs/{file}-{dt}.png", shell=True).wait()
            # size = os.path.getsize(path)
            # print(human_readable(size))
            # load_actual_h5_dataset(path)
            # load_actual_h5_dataset_by_chunks(path)


def get_path(size, scratch_dir=None):
    if scratch_dir:
        return f"{scratch_dir}/{files.dataset_names()[size]}"
    return files.datasets_dict()[size]


def load_size(size, scratch_dir=None):
    return load(get_path(size))


def load_all():
    for path in files.datasets_list():
        load(path)


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else get_path(size, scratch_dir=os.environ["SCRATCHDIR"])
    data = load(path)
    for i in range(n_times):
        data = np.append(data, array_of_elements, axis=0)


if __name__ == '__main__':
    # load_actual_h5_dataset_by_chunks(sys.argv[1])
    load_all()
