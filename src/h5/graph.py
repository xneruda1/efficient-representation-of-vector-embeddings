import os

import pandas as pd
import seaborn as sns
import pandas
import matplotlib.pyplot as plt


def create_metadata(path: str = "../data"):
    metadata = dict()
    for filename in os.listdir(path):
        metadata[filename] = os.path.getsize(path + '/' + filename)

    output = f"{path}/../metadata/sizes.csv"
    with open(output, 'w') as sizes:
        sizes.write('name,size\n')
        for name, size in metadata.items():
            sizes.write(f"{name},{size}\n")
    return output


def dataset_sizes(path: str = "../data",
                  output: str = "../graphs/sizes.png"):
    metadata = create_metadata(path)
    sizes = pandas.read_csv(metadata)
    sns.despine()
    ax = sns.barplot(data=sizes, y='name', x='size')
    ax.set_title('File sizes depending on compression')
    ax.bar_label(ax.containers[0], fmt='%i', padding=5.0)
    sns.despine()
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(output)
    plt.clf()


def dataset_load_time(path: str = '../metadata/time_to_load.csv',
                      output: str = "../graphs/loading_time.png"):
    load_time = pandas.read_csv(path)
    print(load_time)
    ax = sns.barplot(data=load_time, y='name', x='seconds')
    ax.set_title('(10x)Load time depending on compression')
    ax.bar_label(ax.containers[0], fmt='%.3f', padding=5.0)
    sns.despine()
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(output)
    plt.clf()


def comparison_table(output: str = "../graphs/comparison.png"):
    df = pd.DataFrame({'Library': ['Pickle', 'HDF5', 'NumPy', 'TensorFlow', 'Parquet', 'PyTorch'],
                       'Compression': ["No integrated compression",
                                       # https://docs.python.org/3/library/pickle.html#data-stream-format
                                       "gzip, lzf, third party",
                                       # https://docs.h5py.org/en/stable/high/dataset.html#lossless-compression-filters
                                       "zip, bzip2, lzma",
                                       # https://docs.python.org/3/library/zipfile.html
                                       "TODO",
                                       "TODO",
                                       "TODO"],

                       'Chunking': ['no', 'yes', '?', 'yes', '?', '?'],
                       'float16 support': ['yes', 'yes', 'yes', '?', 'No', '?']})

    fig, ax = plt.subplots()
    fig.patch.set_visible(False)
    ax.axis('off')
    ax.axis('tight')
    table = ax.table(cellText=df.values, colLabels=df.columns,
                     loc='center', cellLoc='left',
                     bbox=[0, 0, 1, 0.35],)
    plt.savefig(output, dpi=300)
    plt.clf()


if __name__ == '__main__':
    # dataset_sizes()
    # dataset_load_time()
    comparison_table()
