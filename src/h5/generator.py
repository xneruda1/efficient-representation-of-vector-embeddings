import numpy as np
import h5py
import os

DATA_TYPE = np.float32
N = 50000
DIMENSIONS = 100
FILE_NAME = 'generated_file.h5'


def random_dataset(n: int = N,
                   dimensions: int = DIMENSIONS,
                   data_type: np.number = DATA_TYPE,
                   seed: int = 1234):
    shape = (n, dimensions)
    rng = np.random.default_rng(seed)
    dataset = np.ndarray(shape=shape, dtype=data_type)
    for i in range(n):
        dataset[i] = rng.random(dtype=data_type, size=dimensions)
    return dataset


def random_h5_dataset(n: int = N,
                      dimensions: int = DIMENSIONS,
                      data_type: np.number = DATA_TYPE,
                      file_name: str = FILE_NAME,
                      compression=None):
    shape = (n, dimensions)
    h5file = h5py.File(file_name, 'w')
    h5dset = h5file.create_dataset(name='dataset',
                                   shape=shape,
                                   dtype=data_type,
                                   compression=compression)

    rng = np.random.default_rng(12345)

    for i in range(n):
        h5dset[i, :] = rng.random(dtype=data_type, size=dimensions)
    h5file.close()
    print(f"created new dataset: {file_name}")
    return


def h5_datasets_with_distinct_compressions(path: str = '../data'):
    try:
        os.chdir(path=path)
    except FileNotFoundError:
        os.mkdir(path=path)
        os.chdir(path)
    random_h5_dataset(file_name='no_compression.h5')
    random_h5_dataset(file_name='lzf.h5', compression='lzf')
    random_h5_dataset(file_name=f'gzip.h5', compression='gzip')
    for i in range(0, 10, 2):
        random_h5_dataset(file_name=f'gzip_level_{i}.h5', compression=i)


# TODO: add more third-party compression filters
#  https://portal.hdfgroup.org/display/support/Registered+Filter+Plugins


if __name__ == '__main__':
    h5_datasets_with_distinct_compressions()
