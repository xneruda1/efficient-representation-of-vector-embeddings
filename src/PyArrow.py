import shutil
import sys
import os

import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np
from utils import files


def h5_to_parquet(h5_file: str, output_file: str) -> None:
    import h5py
    with h5py.File(h5_file, 'r') as file:
        data = file['emb'][:]

    table = pa.Table.from_arrays([pa.array(data)], names=['data'])
    pq.write_table(table, output_file)
    print(f"Created {output_file}")


def convert(dataset_key):
    dataset_name = files.datasets_dict()[dataset_key]
    output_file = f"{files.home_data_dir()}parquet/{dataset_key}.parquet"
    h5_to_parquet(dataset_name, output_file)


def convert_all():
    for size in files.dataset_keys():
        print(f"Converting {size}")
        convert(size)


def load(path):
    table = pq.read_table(path)
    return table['data'].to_numpy()


def load_size(size, scratch_dir=None):
    path = get_path(size, home_dir=scratch_dir) if scratch_dir else get_path(size)
    return load(path)


def get_path(size='100K', home_dir=files.home_data_dir()):
    return f"{home_dir}parquet/{size}.parquet"


def load_all():
    for size in files.dataset_keys():
        data = load_size(size)


@files.timer_func
def get_elements(path, first=True, count=100):
    table = pq.read_table(path)
    data = table['data'].to_numpy()
    if first:
        return data[:count]
    return data[-count:]


def scratch_path(size):
    scratch_dir = os.environ.get("SCRATCHDIR", "/tmp")
    return f"{scratch_dir}/{size}.parquet"


@files.timer_func
def get_every_nth(size, n, count):
    table = pq.read_table(scratch_path(size))
    data = table['data'].to_numpy()
    return data[::n][:count]


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


def append_elements(size, arr):
    path = scratch_path(size)
    table = pq.read_table(path)
    combined_data = np.concatenate([table['data'].to_numpy(), arr])
    new_table = pa.Table.from_arrays([pa.array(combined_data)], names=['data'])
    pq.write_table(new_table, path)


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = scratch_path(size) if scratch else get_path(size)
    table = pq.read_table(path)
    data = table['data'].to_numpy()

    for _ in range(n_times):
        data = np.concatenate([data, array_of_elements])

    new_table = pa.Table.from_arrays([pa.array(data)], names=['data'])
    pq.write_table(new_table, path)


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        convert_all()
    else:
        key = sys.argv[1]
        print(key)
        convert(key)
