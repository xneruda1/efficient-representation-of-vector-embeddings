import os

import numpy as np
from zarr.tests.test_filters import compressors

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
import zarr
from numcodecs import blosc
import h5py
from utils import files


def h5_to_zarr(h5_file: str, output_file: str, compression: str = '') -> None:
    with h5py.File(h5_file, "r") as h5_data:
        data = h5_data['emb'][:]
    shape = data.shape
    dtype = data.dtype
    compressor = blosc.Blosc(cname=compression) if compression else None
    print(compressor)
    zarr_array = zarr.create(shape=shape, chunks=True, dtype=dtype, compressor=compressor,
                             store=output_file, overwrite=True)
    zarr_array[:] = data
    print(f"Created Zarr array at {output_file}")


def convert(dataset_key, compression: str = ''):
    dataset_name = files.datasets_dict()[dataset_key]
    output_file = get_path(dataset_key, compression=compression)
    h5_to_zarr(dataset_name, output_file)


def load(path):
    return zarr.open(path)


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def get_path(size='100K', home_dir=files.home_data_dir(), compression=''):
    return f"{home_dir}/zarr/{size}{compression}.zarr"


def get_path_scratch(size):
    scratch_dir = os.environ["SCRATCHDIR"]
    return f"{scratch_dir}/zarr_{size}.zarr"


def load_all():
    for size in files.dataset_keys():
        path = get_path(size)
        data = load(path)


def get_elements_zarr(dataset_path, first=True, count=100):
    data = load(dataset_path)
    if first:
        return data[:count]
    return data[-count:]


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


@files.timer_func
def append_zarr(size, array_of_elements, n_times=1, scratch=True):
    path = get_path(size) if not scratch else get_path_scratch(size)
    zarr_group = zarr.open(path, mode='a')
    print(zarr_group.info)
    zarr_array = zarr_group['emb']
    for i in range(n_times):
        new_data = np.concatenate([zarr_array[:], array_of_elements])
        zarr_array.resize(new_data.shape)
        zarr_array[:] = new_data


if __name__ == "__main__":
    dataset_key = '100K'
    for compr in blosc.list_compressors():
        convert(dataset_key, compression=compr)

    # from h5.generator import random_dataset
    #
    # data = random_dataset(1000, 768)
    # append_zarr(get_path(dataset_key), data, n_times=1)
    # append_zarr(get_path(dataset_key), data, n_times=10)
    # append_zarr(get_path(dataset_key), data, n_times=100)
