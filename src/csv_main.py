# baseline
import csv
import h5py

from src.utils import files


def h5_to_csv(h5_file: str) -> None:
    h5_data = h5py.File(h5_file, "r")
    data = h5_data['emb'][:]
    output_file = get_path()
    with open(output_file, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file)
        for row in data:
            csv_writer.writerow(row)
    print(f"Created {output_file}")
    h5_data.close()


def get_path(size='100K', home_dir=files.home_data_dir()):
    return f"{home_dir}/csv/csv_{size}.csv"


def convert(dataset_key):
    dataset_name = files.datasets_dict()[dataset_key]
    output_file = get_path(dataset_key)
    h5_to_csv(dataset_name)
