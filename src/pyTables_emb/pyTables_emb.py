import shutil
import sys
import os

import h5py
import tables
import numpy as np
from utils import files

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"


def h5_to_tables(h5_file: str, output_file: str, filters: tables.Filters = None) -> None:
    file = h5py.File(h5_file, 'r')
    data = file['emb'][:]
    file.close()
    with tables.open_file(output_file, mode='w') as h5_output:
        atom = tables.Atom.from_dtype(data.dtype)
        output_array = h5_output.create_carray(h5_output.root, 'data',
                                               atom=atom,
                                               shape=data.shape,
                                               filters=filters)
        output_array[:] = data
    print(f"Created {output_file}")


def convert(dataset_key):
    dataset_name = files.datasets_dict()[dataset_key]
    output_file = f"{files.home_data_dir()}pytables/{dataset_key}.h5"
    h5_to_tables(dataset_name, output_file)


def convert_all():
    for size in files.dataset_keys():
        print(f"converting {size}")
        convert(size)


def load(path):
    with tables.open_file(path, mode='r') as h5_data:
        data = h5_data.root.data.read()
    return data


def load_size(size, scratch_dir=None):
    if scratch_dir:
        return load(get_path(size, home_dir=scratch_dir))
    return load(get_path(size))


def get_path(size='100K', home_dir=files.home_data_dir()):
    return f"{home_dir}pytables/{size}.h5"


def load_all():
    for size in files.dataset_keys():
        data = load_size(size)


@files.timer_func
def get_elements(path, first=True, count=100):
    with tables.open_file(path, mode='r') as h5_data:
        data = h5_data.root.data.read()
        if first:
            return data[:count]
        return data[-count:]


def scratch_path(size):
    scratch_dir = os.environ["SCRATCHDIR"]
    return f"{scratch_dir}/{size}.h5"


@files.timer_func
def get_every_nth(size, n, count):
    with tables.open_file(scratch_path(size), mode='r') as h5_data:
        data = h5_data.root.data
        for i in range(0, len(data) - count, n):
            elements = data.read(start=n, stop=n + count)


def copy_to_scratch(dataset_key, scratch_dir):
    src_path = get_path(dataset_key)
    files.copy_to_scratch(src_path, scratch_dir)


def append_random_elements(size, count):
    rng = np.random.default_rng(12345)
    arr = np.ndarray(rng.random(dtype=np.float16, size=(count, 768)))
    append(size, arr)


def append_elements(size, arr):
    append(size, arr)


@files.timer_func
def append(size, array_of_elements, n_times=1, scratch=True):
    path = scratch_path(size) if scratch else get_path(size)
    with tables.open_file(path, mode='w') as h5_data:
        data = h5_data.root
        print(list(data))
        for i in range(n_times):
            h5_data.root.append(array_of_elements)


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        convert_all()
    else:
        key = sys.argv[1]
        print(key)
        convert(key)
